extends KinematicBody

const max_tilt = 0.7
const max_rotate = 0.045
const dead_zone = 0.15
const pitch_reverse_flag = false

export var speed = 10.0
export var accel = 2
export var deaccel = 8
export var max_slope_angle = 30

var velocity = Vector3( 0, 0, 0 )
var move_vector = Vector3( 0, 0, 0 )
var rotate_speed = Vector3( 0, 0, 0 )

func _ready():
	self.transform = Transform( )

func _physics_process( delta ):
	# Input
	self._get_input( )

	# * Translation Move *
	var hvel = self.velocity
	var target = self.move_vector * speed
	var current_accel = self.accel
	if 0 < self.move_vector.dot( hvel ):
		current_accel = self.deaccel
	hvel = hvel.linear_interpolate( target, current_accel * delta )
	self.velocity = self.move_and_slide( hvel, Vector3( 0, 1, 0 ) )

	# * Rotate *
	var basis = self.transform.basis.orthonormalized( )
	# Roll
	basis = basis.rotated( basis.z, self.rotate_speed.z * max_rotate )
	# Yaw
	basis = basis.rotated( basis.y, self.rotate_speed.y * max_rotate )
	# Pitch
	self.transform.basis = basis.rotated( basis.x, self.rotate_speed.x * max_rotate )

func _get_input( ):
	var pitch_reverse = -1
	if pitch_reverse_flag:
		pitch_reverse = 1

	var basis = self.transform.basis.orthonormalized( )
	self.move_vector = Vector3( 0, 0, 0 )
	self.rotate_speed = Vector3( 0, 0, 0 )

	# Left Stick (Translation Move)
	var ax = Input.get_joy_axis( 0, 0 )
	if dead_zone < abs( ax ):
		self.move_vector += basis.x * ax
	var az = Input.get_joy_axis( 0, 1 )
	if dead_zone < abs( az ):
		# Press L1 to up/down, Release L1 to forward/backward
		if Input.is_action_pressed( "switch_slide" ):
			self.move_vector += basis.y * az * pitch_reverse
		else:
			self.move_vector += basis.z * az

	# Right Stick (Yaw and Pitch)
	var by = Input.get_joy_axis( 0, 2 )
	if dead_zone < abs( by ):
		self.rotate_speed.y = -by
	var bx = Input.get_joy_axis( 0, 3 )
	if dead_zone < abs( bx ):
		self.rotate_speed.x = bx * pitch_reverse

	# Xbox R2 L2 (Roll)
	if Input.is_action_pressed( "roll_left" ):
		self.rotate_speed.z = 1
	if Input.is_action_pressed( "roll_right" ):
		self.rotate_speed.z = -1
