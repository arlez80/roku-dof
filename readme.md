# ろくどふ / Roku DoF

Godot Engineの6DoFサンプルプロジェクト。

This is sample project of 6DoF FPS game.

## How to control

(I check XBox360 controller only...)

* Forward/Backward/Left/Right : Left Stick
* Up/Down slide: Left Stick with L1
* Yaw/Pitch: Right Stick
* Roll: L2 R2

## License

Public domain
